<?php
defined("ABSPATH") or die();

add_action("wp_ajax_gdebugger_clear_log", "gdebugger_clear_log_func");
function gdebugger_clear_log_func() {
	update_option("gdebugger_log", array());
	gdebugger_get_log_func();
	wp_die();
}

add_action("wp_ajax_gdebugger_get_log", "gdebugger_get_log_func");
function gdebugger_get_log_func() {
	$content = get_option("gdebugger_log");
	
	if ( is_array($content) && count($content) > 0 ) {
		foreach ( $content as $item ) {
		?>
		    <tr class="alternate">
		        <td><?php echo date("d/m/Y", $item["date"]) ?></td>
		        <td colspan="6"><?php echo strip_tags($item["content"], "<pre>") ?></td>
		    </tr>
		<?php
		}
	} else {
		?>
		    <tr class="alternate">
		        <td colspan="7">No records yet...</td>
		    </tr>
		<?php
	}
	
	if (defined('DOING_AJAX') && DOING_AJAX) wp_die();
}

add_action("wp_ajax_gdebugger_install_free_testing_page", "gdebugger_install_free_testing_page_func");
function gdebugger_install_free_testing_page_func() {
	$location = explode("/plugins/", __FILE__);
	$themeLoc = explode("/themes/", get_template_directory_uri());
	
	$testingPage = get_posts(array("post_type"=>"page", "meta_key"=>"_wp_page_template", "meta_value"=>"testing.php", "posts_per_page"=>1));
	if ( count($testingPage) == 0 ) {
		$pageID = wp_insert_post(array(
			"post_title"=>"GDebugger Testing Page",
			"post_status"=>"publish",
			"post_type"=>"page"
		));
		update_post_meta($pageID, "_wp_page_template", "testing.php");
	} else {
		$pageID = $testingPage[0]->ID;
	}
	
	if ( !file_exists($location[0] . "/themes/" . $themeLoc[1] . "/testing.php") ) {
		$testPagePermalink = get_permalink($pageID);
		$wocommerceVar = '$woocommerce';
		$fileContent = "<?php
/*
Template Name: GDebugger Free Testing Page

URL: {$testPagePermalink}
*/

// You can remove this line and below...
if ( function_exists('g') ) {
	g('<b>This is a free testing page - you can test and check anything you want here!</b>', '<br />');
	
	g('<b>Want to see the current user details? No problem:</b>', wp_get_current_user(), '<br />');
	
	if ( class_exists('WooCommerce') ) {
		g('<b>Want to see current cart content in WooCommerce? Just type it!</b>');
		global {$wocommerceVar};
		g( {$wocommerceVar}->cart->get_cart() );
	}
}
";
		file_put_contents($location[0] . "/themes/" . $themeLoc[1] . "/testing.php", $fileContent);
	}
	
	if (defined('DOING_AJAX') && DOING_AJAX) wp_die();
}

add_action("wp_ajax_gdebugger_uninstall_free_testing_page", "gdebugger_uninstall_free_testing_page_func");
function gdebugger_uninstall_free_testing_page_func() {
	$location = explode("/plugins/", __FILE__);
	$themeLoc = explode("/themes/", get_template_directory_uri());
	if ( file_exists($location[0] . "/themes/" . $themeLoc[1] . "/testing.php") ) unlink($location[0] . "/themes/" . $themeLoc[1] . "/testing.php");
	
	$testingPage = get_posts(array("post_type"=>"page", "meta_key"=>"_wp_page_template", "meta_value"=>"testing.php", "posts_per_page"=>1));
	if ( count($testingPage) > 0 ) {
		wp_delete_post( $testingPage[0]->ID, true );
	}
	
	wp_die();
}

add_action("wp_ajax_gdebugger_save_testing_page", "gdebugger_save_testing_page_func");
function gdebugger_save_testing_page_func() {
	$location = explode("/plugins/", __FILE__);
	$themeLoc = explode("/themes/", get_template_directory_uri());
	file_put_contents($location[0] . "/themes/" . $themeLoc[1] . "/testing.php", stripslashes($_POST["content"]));

	wp_die();
}