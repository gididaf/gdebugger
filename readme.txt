=== GDebugger ===
Contributors: gididaf
Tags: debug, debugger, log, print_r, test, testing
Requires at least: 2
Tested up to: 4.3.1
Stable tag: 4.3.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Cool and small debugger to print variables on the screen the pretty way and easy debugging ajax / Wordpress hooks with live log.

== Description ==

Wordpress developers! got tired of writing - echo '<pre>'; print_r($var); echo '</pre>'; anytime to check a simple variable?
Feel lost when need to debug ajax functions / Wordpress hooks?

Meet g() - the magical function!

With g() you can:

*   Check variable(s) content really easy.
*   Easy debugging ajax calls and Wordpress hooks with the live-log.
*   Super easy to use, just type g() and start debugging

Debugging variables never be that easy:

*   g($var, $array, $object);
*   g($var1, $var2, $var3, S); - stop the script immediately.
*   $res = g($var1, $var2, R); - return result instead of print it.
*   g(L); - start writing everything to the log from this point.
*   g(LE); - stop writing everything to the log.

== Installation ==

1. Install the plugin.
1. Activate the plugin through the 'Plugins' menu in WordPress.
1. Take couple of minutes to read the Help page - it's important!
1. Start using the g() function everywhere.

== Frequently Asked Questions ==

= What? Only 1 function? =

Yep! Everything you need is inside the g()! We also defined a couple of constants for you to use inside the function.

= I don't think i understand what this plugin do =

Please take a couple of minutes to read the Help page and i promise you will understand everything.

== Screenshots ==

1. The live-log
2. The settings page
3. Part of the Help page

== Changelog ==

= 1.0 =
* First release!

