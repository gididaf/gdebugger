<?php
/*
Plugin Name: GDebugger
Description: Debugger tool for Wordpress
Version:     1.0
Author:      Gidi Dafner
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

defined("ABSPATH") or die();

register_activation_hook(__FILE__, "gdebugger_install_the_testing_page_func");
function gdebugger_install_the_testing_page_func() {
	gdebugger_install_free_testing_page_func();
	update_option("gdebugger_status", "1");
}

if ( function_exists("g") ) {
	
	add_action("admin_notices", "gdebugger_cant_load_func");
	function gdebugger_cant_load_func() {
	?>
    <div class="error" style="direction: ltr">
	    <h3>GDebugger Cannot Be Activated</h3>
        <h4>- There is already defined function called 'g'.</h4>
    </div>
    <?php
	}
	
} else {

	require_once plugin_dir_path(__FILE__) . "log.php";
	require_once plugin_dir_path(__FILE__) . "settings.php";
	require_once plugin_dir_path(__FILE__) . "help.php";
	require_once plugin_dir_path(__FILE__) . "ajax.php";
	require_once plugin_dir_path(__FILE__) . "testing_page_editor.php";
	
	$gdebuggerStatus = get_option("gdebugger_status");
	
	if ($gdebuggerStatus) {
		if ( !defined("S") ) define("S", "GDEBUGGER_STOP_FUNCTION_CONSTANT");
		if ( !defined("R") ) define("R", "GDEBUGGER_RETURN_FUNCTION_CONSTANT");
		if ( !defined("L") ) define("L", "GDEBUGGER_LOG_FUNCTION_CONSTANT");
		if ( !defined("LE") ) define("LE", "GDEBUGGER_LOG_END_FUNCTION_CONSTANT");
		if ( !defined("LC") ) define("LC", "GDEBUGGER_LOG_END_CLEAR_FUNCTION_CONSTANT");
		
		function g() {
			$args = func_get_args();
			if ( count($args) == 1 && $args[0] == L ) { gdebugger_log_mode(1); return; }
			if ( count($args) == 1 && $args[0] == LE ) { gdebugger_log_mode(0); return; }
			if ( count($args) == 1 && $args[0] == LC ) { gdebugger_log_mode(0, true); return; }
			
			$return = false; $die = false; $log = false; $logClear = false;
			ob_start();
			foreach ( $args as $arg ) {
				if ( $arg == S ) { $die = true; continue; }
				if ( $arg == R ) { $return = true; continue; }
				if ( $arg == L ) { $log = true; continue; }
				if ( $arg == LC ) { $log = true; $logClear = true; continue; }
				echo "<pre>"; print_r($arg); echo "</pre>";
			};
			if ( $log ) {
				$content = ob_get_contents();
				$prevContent = get_option("gdebugger_log");
				if ( !$prevContent || !is_array($prevContent) || $logClear ) $prevContent = array();
				$prevContent[] = array("date"=>time(), "content"=>$content);
				update_option("gdebugger_log", $prevContent);
			}
			if ( $die ) { ob_get_flush(); die; }
			if ( $return ) return ob_get_clean();
			if ( $log ) { ob_end_clean(); return; }
			ob_get_flush();
		}
		
		function gdebugger_log_mode($status, $clear = false) {
			global $gdebugger_log_mode_var;
			if ( $status == 1 && !isset($gdebugger_log_mode_var) ) {
				$gdebugger_log_mode_var = true;
				ob_start();
			}
			
			if ( $status == 0 && isset($gdebugger_log_mode_var) ) {
				unset($gdebugger_log_mode_var);
				$content = ob_get_clean();
				$prevContent = get_option("gdebugger_log");
				if ( !$prevContent || !is_array($prevContent) || $clear ) $prevContent = array();
				$prevContent[] = array("date"=>time(), "content"=>$content);
				update_option("gdebugger_log", $prevContent);
				echo $content;
			} elseif ( $status == 0 && !isset($gdebugger_log_mode_var) && $clear ) {
				update_option("gdebugger_log", array());
			}
		}
		
		add_action("wp_footer", "gdebugger_save_log_if_user_forgot_func");
		function gdebugger_save_log_if_user_forgot_func() {
			global $gdebugger_log_mode_var;
			if ( isset($gdebugger_log_mode_var) ) gdebugger_log_mode(0);
		}
		
	}
	
	add_action("plugin_action_links_" . plugin_basename(__FILE__), "gdebugger_add_action_to_plugins_page_func");
	function gdebugger_add_action_to_plugins_page_func($links) {
		$links[] = "<a href='" . menu_page_url("gdebugger_help_menu", false) . "'>Help</a>";
		return $links;
	}
	
	add_action("admin_menu", "gdebugger_create_menu_page_func");
	function gdebugger_create_menu_page_func() {
		add_menu_page("GDebugger", "GDebugger", "manage_options", "gdebugger_main_menu", "gdebugger_log_page_content_func", "", "75.4565");
		add_submenu_page("gdebugger_main_menu", "Log", "Log", "manage_options", "gdebugger_main_menu");
		add_submenu_page("gdebugger_main_menu", "Settings", "Settings", "manage_options", "gdebugger_settings_menu", "gdebugger_settings_page_content_func");
		add_submenu_page("gdebugger_main_menu", "Help", "Help", "manage_options", "gdebugger_help_menu", "gdebugger_help_page_content_func");
		add_submenu_page(null, "Testing Page Editor", "Testing Page Editor", "manage_options", "gdebugger_testing_page_editor_menu", "gdebugger_testing_editor_page_content_func");
	}
	
}
