<?php
defined("ABSPATH") or die();

function gdebugger_help_page_content_func() {
	$location = explode("/plugins/", __FILE__);
	$themeLoc = explode("/themes/", get_template_directory_uri());
?>
	<style>
		.wrap {direction: ltr;}
		.postbox {position: relative;}
		.tldr {position: absolute; right: 4px; top: 4px; border: 1px solid #e5e5e5; padding: 6px;}
		.tldr div.info {line-height: 17px;}
	</style>
	
	<div class="wrap">
		<h2> GDebugger Help </h2> <br />
		<div class="postbox">
			<div class="tldr">
				<u>TL;DR</u><br /><br />
				<div class="info">
					g($var1, [$var2 ... $varN]) - print; <br />
					g($var1, [$var2 ... $varN], S) - print and die; <br />
					g($var1, [$var2 ... $varN], R) - return instead of print; <br />
					g(L) - start live-log buffer; <br />
					g(LE) - close live-log buffer; <br />
					g(LC) - close and clear live-log buffer; <br />
				</div>
			</div>
			
			<div class="inside">
				<h3><u> Meet the magical g() function... </u></h3>
				<div class="inside">
					
					<p>
						- Use g() to print variable content on the screen: <b> g($var1); </b> <br />
						- You can pass multiply parameters: <b> g($var1, $var2, $var3); </b>
					</p>
					<p>
						<b><u>Example:</u></b>
						<div class="inside">
<pre>
   $var1 = 5;
   $var2 = array(1, 2, 3);
   $var3 = array("name"=>"Gidi", "age"=>28, "books"=>array("Batman", "Superman", "Rambo"));
	
   <b>g($var1, $var2, $var3);</b>
</pre>
						</div>
						<b><u>Result:</u></b>
						<div class="inside">
							<?php g(5, array(1, 2, 3), array("name"=>"Gidi", "age"=>28, "books"=>array("Batman", "Superman", "Rambo"))); ?>
						</div>
					</p>
					<p>
						- You can use the defined R to return the content instead of print it to the screen: <b>$printed = g($var1, $var2, R);</b>
					</p>
					
					<hr />
					
					<p>
						- You can use the defined S to stop the script immediately: <b>g($var1, $var2, S);</b>
					</p>
					<p>
						<b><u>Example:</u></b>
						<div class="inside">
<pre>
   $var1 = "Banana";
   <b>g($var1, S);</b>
   $var2 = "Orange";
   <b>echo = g($var2, R);</b> // this code will never execute
</pre>
						</div>
						<b><u>Result:</u></b>
						<div class="inside">
							<?php $var1 = "Banana"; g($var1); ?>
						</div>
					</p>
					
					<hr />
					
					<p>
						- GDebugger comes with a <a href="<?php echo menu_page_url("gdebugger_main_menu", false) ?>">live-update log</a> - use it for hard-to-debug sections like ajax calls and wordpress hooks!
						- Use <b>g(L)</b> to start the logging and <b>g(LE)</b> to stop the logging and see the result in the log page.
					</p>
					<p>
						<b><u>Example:</u></b>
						<div class="inside">
<pre>
   <b>g(L);</b>
   $var1 = pow(2, 4);
   g($var1);
   $var2 = pow(3,2) * $var1;
   g($var2);
   <b>g(LE);</b>
</pre>
						</div>
						<b><u>Result:</u></b>
						<div class="inside">
							You will see the numbers 16 and 166 immediately in the log page.
						</div>
					</p>
					<p>
						- You can use L with any variable(s) together to write them immediately to the log: <b>g($var1, L);</b> <br />
						- You can use LC with any variable(s) together to clear the log and then write the variable(s): <b>g($var1, LC);</b> <br />
						- You can use the defined LC to clear the log before:
<pre>
   <b>g(L);</b>
   [some echo's...];
   <b>g(LC);</b>
</pre>
					</p>
					
				</div>
				
				<hr />
				
				<h3 id="helpTestingPage"><u> Bonus - Free Testing Page </u></h3>
				<div class="inside">
					After you installed the testing page via <a target="_blank" href="<?php menu_page_url("gdebugger_settings_menu") ?>">Settings</a>, open the file <b>testing.php</b> in your template directory:<br />
					Full path is <b><?php echo $location[0] . "/themes/" . $themeLoc[1] . "/testing.php" ?></b> <br />
					You can also use the <a target="_blank" href="<?php menu_page_url("gdebugger_testing_page_editor_menu") ?>">build-in editor.</a><br /><br />
					In the testing page you can write and check whatever you want! It's awesome for debugging at developing. You can check the current user session, current WooCommerce cart, small pieces of code that you want to debug before insert to an AJAX function, and much more! <br /><br />
					Useful functions to debug in the testing page for example:
<pre>
   g( wp_get_current_user() );
   g( WC()->cart->get_cart() );
   g( $_SESSION );
   g( get_posts(array("post_type"=>"cars", "posts_per_page"=>-1)) );
   
   g(L);
   Function_With_Hooks_We_Want_To_Debug();
   g(LE);
</pre>
				</div>
				
			</div> <!-- inside -->
		</div> <!-- postbox -->
	</div> <!-- wrap -->
<?php
}