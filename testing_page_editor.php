<?php
	
function gdebugger_testing_editor_page_content_func() {
	$location = explode("/plugins/", __FILE__);
	$themeLoc = explode("/themes/", get_template_directory_uri());
	$fileContents = file_get_contents($location[0] . "/themes/" . $themeLoc[1] . "/testing.php");
?>
	<style>
		.wrap {direction: ltr;}
		textarea {width: 100%; height: 500px;}
	</style>

	<div class="wrap">
		<h2> Testing Page Editor </h2> <br />
		<p>This page located at: <b><?php echo $location[0] . "/themes/" . $themeLoc[1] . "/testing.php" ?></b></p>
		<textarea id="txtContent"><?php echo $fileContents ?></textarea>
		<p class="submit"><input type="submit" name="savePage" id="savePage" class="button button-primary" value="Save"></p>
	</div>
	
	<script>
		jQuery("#savePage").click( function() {
			jQuery("#savePage").attr("disabled", "disabled");
			jQuery.post(ajaxurl, {action: "gdebugger_save_testing_page", content: jQuery("#txtContent").val().trim()}, function(data) {
				alert("testing.php updated succesfully.");
				jQuery("#savePage").removeAttr("disabled");
			});
		});
	</script>
<?php
}