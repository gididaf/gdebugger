<?php
if ( !defined("WP_UNINSTALL_PLUGIN") ) exit();

$location = explode("/plugins/", __FILE__);
$themeLoc = explode("/themes/", get_template_directory_uri());
if ( file_exists($location[0] . "/themes/" . $themeLoc[1] . "/testing.php") ) unlink($location[0] . "/themes/" . $themeLoc[1] . "/testing.php");

$testingPage = get_posts(array("post_type"=>"page", "meta_key"=>"_wp_page_template", "meta_value"=>"testing.php", "posts_per_page"=>1));
if ( count($testingPage) > 0 ) {
	wp_delete_post( $testingPage[0]->ID, true );
}

delete_option("gdebugger_status");
delete_option("gdebugger_log");