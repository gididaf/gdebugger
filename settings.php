<?php
defined("ABSPATH") or die();

function gdebugger_settings_page_content_func() {
	if ( isset($_POST["turn"]) && $_POST["turn"] == "0" ) update_option("gdebugger_status", "0");
	if ( isset($_POST["turn"]) && $_POST["turn"] == "1" ) update_option("gdebugger_status", "1");
	
	$gdebuggerStatus = get_option("gdebugger_status");
	
	$location = explode("/plugins/", __FILE__);
	$themeLoc = explode("/themes/", get_template_directory_uri());
	
	$testingPage = get_posts(array("post_type"=>"page", "meta_key"=>"_wp_page_template", "meta_value"=>"testing.php", "posts_per_page"=>1));
	$testingPageExists = ( file_exists($location[0] . "/themes/" . $themeLoc[1] . "/testing.php") && count($testingPage) > 0 );
	
	$pluginData = get_plugin_data( plugin_dir_path(__FILE__) . "gdebugger.php" );
	$pluginVersion = $pluginData["Version"];
?>
	<style>
		.wrap {direction: ltr;}
		
		span.on {color: green; font-weight: bold;}
		span.off {color: red; font-weight: bold;}
		
		.display-inline-block {display: inline-block;}
		.vertical-align-middle {vertical-align: middle;}
		
		table {width: 100%;}
		table tr td.first {width: 200px;}
	</style>

	<div class="wrap">
		<h2> GDebugger Settings </h2> <br />
		<div class="postbox">
			<div class="inside">
				<table>
					<tr>
						<td class="first"> GDebugger Status: <span class="<?php echo $gdebuggerStatus ? "on" : "off" ?>"><?php echo $gdebuggerStatus ? "ON" : "OFF" ?></span> </td>
						<td>
							<form method="post" onsubmit="<?php echo $gdebuggerStatus ? "return confirm('Are you sure?')" : "" ?>">
								<input type="hidden" name="turn" value="<?php echo $gdebuggerStatus ? "0" : "1" ?>" />
								<input type="submit" class="button button-primary" value="Turn <?php echo $gdebuggerStatus ? "Off" : "On" ?>" />
							</form>
						</td>
					</tr>
					<tr>
						<td class="first"> Free Testing Page: <span class="<?php echo $testingPageExists ? "on" : "off" ?>"><?php echo $testingPageExists ? "Exists" : "Not Exists" ?></span> </td>
						<td>
							<div class="display-inline-block vertical-align-middle">
								<?php if ($testingPageExists) { ?>
								<a target="_blank" href="<?php echo $testingPage[0]->guid ?>"><input type="submit" class="button button-primary" value="Open" /></a>
								<a target="_blank" href="<?php menu_page_url("gdebugger_testing_page_editor_menu") ?>"><input type="submit" class="button button-primary" value="Edit" /></a>
								<input id="btnUninstallTestingPage" type="submit" class="button button-primary" value="Uninstall" />
								<?php } else { ?>
								<input id="btnInstallTestingPage" type="submit" class="button button-primary" value="Install" />
								<?php } ?>
							</div>
							<a target="_blank" href="<?php menu_page_url("gdebugger_help_menu") ?>#helpTestingPage">What is that?</a>
						</td>
					</tr>
					<tr>
						<td colspan="2"><hr /></td>
					</tr>
					<tr>
						<td class="first"> Current Version: <?php echo $pluginVersion ?> </td>
						<td>
							<input id="btnCheckForUpdates" type="submit" class="button button-primary" value="Check For Updates" />
						</td>
					</tr>
					<tr>
						<td class="first"> Need To Contact Me? </td>
						<td>
							<a href="mailto:gididaf1@gmail.com"><input type="submit" class="button button-primary" value="Send Me An Email!" /></a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	
	<script>
		(function($) {
			$("#btnInstallTestingPage").click( function() {
				$("#btnInstallTestingPage").val("Installing...").attr("disabled", "disabled");
				$.post(ajaxurl, {action: "gdebugger_install_free_testing_page"}, function(data) {
					location.reload();
				});
			});
			
			$("#btnUninstallTestingPage").click( function() {
				if ( !confirm("Are you sure?") ) return;
				
				$("#btnUninstallTestingPage").val("Uninstalling...").attr("disabled", "disabled");
				$.post(ajaxurl, {action: "gdebugger_uninstall_free_testing_page"}, function(data) {
					location.reload();
				});
			});
		})(jQuery);
	</script>
<?php
}