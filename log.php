<?php
defined("ABSPATH") or die();

function gdebugger_log_page_content_func() {
?>
	<style>
		.wrap {direction: ltr;}

		#frmGDebuggerLog table thead tr th, #frmGDebuggerLog table tbody tr td {text-align: left;}
		#frmGDebuggerLog p.submit {text-align: left;}
	</style>

	<div class="wrap">
		<h2> GDebugger Log </h2> <br />
		<form id="frmGDebuggerLog" onsubmit="event.preventDefault()">
			<table class="widefat fixed">
			    <thead>
				    <tr>
			            <th class="manage-column"> Date & Time </th>
			            <th class="manage-column" colspan="6"> Content </th>
				    </tr>
			    </thead>
			    <tbody class="logBody">
				    <?php gdebugger_get_log_func(); ?>
			    </tbody>
			</table>
			
			<p> <label> <input type="checkbox" id="gb_debugger_prevent_autoupdate"> Prevent Auto Updating Log </label> </p>
			<p class="submit"><input type="submit" name="clearLog" id="clearLog" class="button button-primary" value="Clear Log"></p>
		</form>
	</div>
	
	<script>
		var gdebugger_timer;
		var gdebugger_auto_update_log = true;
		
		jQuery(document).ready( function() {
			jQuery("#clearLog").click( function() {
				gdebugger_clearTimer();
				jQuery("#clearLog").attr("disabled", "disabled").text("Working...");
				jQuery("#gb_debugger_prevent_autoupdate").attr("disabled", "disabled");
				
				jQuery.post(ajaxurl, {action: "gdebugger_clear_log"}, function(data) {
					jQuery("#frmGDebuggerLog .logBody").html(data);
					jQuery("#clearLog").removeAttr("disabled").text("Clear Log");
					jQuery("#gb_debugger_prevent_autoupdate").removeAttr("disabled");
					if ( gdebugger_auto_update_log ) gdebugger_setTimer();
				});
				
				jQuery("html, body").animate({scrollTop: 0}, "fast");
			});
			
			jQuery("#gb_debugger_prevent_autoupdate").click( function() {
				if ( jQuery(this).prop("checked") ) {
					gdebugger_auto_update_log = false;
					gdebugger_clearTimer();
				} else {
					gdebugger_auto_update_log = true;
					gdebugger_setTimer();
				}
			});
			
			gdebugger_setTimer();
		});
		
		function gdebugger_refresh_log() {
			gdebugger_clearTimer();
			jQuery.post(ajaxurl, {action: "gdebugger_get_log"}, function(data) {
				if ( jQuery("#frmGDebuggerLog .logBody").html() != data ) jQuery("#frmGDebuggerLog .logBody").html(data);
				if ( gdebugger_auto_update_log ) gdebugger_setTimer();
			});
		}
		
		function gdebugger_setTimer() {
			if ( !gdebugger_timer ) {
				gdebugger_timer = setTimeout(gdebugger_refresh_log, 1000);
			}
		}
		
		function gdebugger_clearTimer() {
			if ( gdebugger_timer ) {
				clearTimeout(gdebugger_timer);
				gdebugger_timer = null;
			}
		}
	</script>
<?php
}
